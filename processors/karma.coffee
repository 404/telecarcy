karmaController = require '../controllers/karma'

exports.increment = (msg, cmd, tg) ->
  name = cmd.regex.exec(msg.text)[1]
  chat = msg.chat.id
  karmaController.increment chat, name, (err, karma) ->
    msg = "#{karma.name} now has karma #{karma.quantity}"
    tg.sendMessage({text: msg, chat_id: chat})

exports.decrement = (msg, cmd, tg) ->
  name = cmd.regex.exec(msg.text)[1]
  chat = msg.chat.id
  karmaController.decrement chat, name, (err, karma) ->
    msg = "#{karma.name} now has karma #{karma.quantity}"
    tg.sendMessage({text: msg, chat_id: chat})

exports.show = (msg, cmd, tg) ->
  chat = msg.chat.id
  karmaController.getAll chat, (err, karmas) ->
    ret = ''
    for karma in karmas
      ret += "#{karma.name}  = #{karma.quantity}, " if karma.quantity isnt 0
    msg = 'karmas: ' + ret.substring(0, ret.length - 2)
    tg.sendMessage({text: msg, chat_id: chat})