exports.process = (msg, cmd, tg) ->
  chat = msg.chat.id
  reply = cmd.reply
  if(cmd.groups)
    i = 0
    match = cmd.regex.exec msg.text
    reply = reply.replace /{}/g, () ->
       match[cmd.groups[i++]] ? ''

  tg.sendMessage({text: reply, chat_id: chat})