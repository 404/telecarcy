learnController = require '../controllers/learn'

exports.add = (msg, cmd, tg) ->
  data = cmd.regex.exec(msg.text)
  regex = data[1]
  reply = data[2]
  groups = data[3].split(',').map(Number) if data[3]
  learnController.add regex, reply, groups, (err) ->
    if err
      text = err
    else
      text = "saquei!"
      global.commands.push {
        regex: new RegExp(regex, "i"),
        reply: reply,
        groups: groups
      }
    tg.sendMessage({text: text, chat_id: msg.chat.id})

exports.del = (msg, cmd, tg) ->
  regex = cmd.regex.exec(msg.text)[1]
  learnController.del regex, (err, reply) ->
    if reply
      text = 'esqueci!'
      global.commands = global.commands.filter (del) ->
        str1 = String del.regex
        str2 = String new RegExp(regex, "i")
        str1.valueOf() isnt str2.valueOf()
    else
      text = 'nem lembrava disso :('
    tg.sendMessage({text: text, chat_id: msg.chat.id})

exports.show = (msg, cmd, tg) ->
  chat = msg.chat.id
  learnController.getAll (err, replies) ->
    ret = 'learns:\n'
    for reply in replies
      ret += "\"#{reply.regex}\" -> \"#{reply.reply}\""
      if reply.groups and reply.groups.length > 0
        ret += "-> \"#{reply.groups}\""
      ret += "\n"

    tg.sendMessage({text: ret, chat_id: chat})