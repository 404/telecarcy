express = require 'express'
global.commands = require('../commands').commands
Telegram = require 'telegram-bot'
chat = require '../processors/chat'
router = express.Router()
tg = new Telegram(process.env.TELEGRAM_BOT_TOKEN)

router.post '/', (req, res) ->
  return res.send 200 unless req.body.message.text
  msg = req.body.message;
  for cmd in global.commands
    if cmd.regex.exec(msg.text)?
      if cmd.processor then cmd.processor(msg, cmd, tg) else chat.process(msg, cmd, tg)
      break
  res.sendStatus 200

module.exports = router;