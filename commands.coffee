karma = require './processors/karma'
learn = require './processors/learn'
learnController = require './controllers/learn'

commands = [
  regex: /\/learn \"(.*)\" \"(.*)\" \"(.*)\"/
  processor: learn.add
,
  regex: /\/learn \"(.*)\" \"(.*)\"/
  processor: learn.add
,
  regex: /\/forget \"(.*)\"/
  processor: learn.del
,
  regex: /\/learns/
  processor: learn.show
,
  regex: /\/karmas/
  processor: karma.show
,
  regex: /([A-zÀ-ÿ]([A-zÀ-ÿ]|[._-])+)\+\+/
  processor: karma.increment
,
  regex: /([A-zÀ-ÿ]([A-zÀ-ÿ]|[._-])+)\-\-/
  processor: karma.decrement
]

learnController.getAll (err, replies) ->
  for reply in replies
    commands.push {
      regex: new RegExp(reply.regex, "i"),
      reply: reply.reply,
      groups: reply.groups
    }

exports.commands = commands;
