mongoose = require 'mongoose'

port = process.env.PORT or 3000
db_host = process.env.DBHOST or "localhost"
db_name = process.env.DBNAME or 'telecarcy'
db_port = process.env.DBPORT or 27017
db_url = "mongodb://#{db_host}:#{db_port}/#{db_name}"

mongoose.connect(db_url)

http = require 'http'
express = require 'express'
path = require 'path'
logger = require 'morgan'
cookieParser = require 'cookie-parser'
bodyParser = require 'body-parser'
telegram = require './routes/telegram'

app = express()

app.use logger 'dev'
app.use bodyParser.json()
app.use bodyParser.urlencoded { extended: false }
app.use cookieParser()

app.use '/telegram', telegram

app.set 'port', port
server = http.createServer app
server.listen port