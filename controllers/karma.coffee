Karma = require '../models/karma'

findKarma =  (chat, name, callback) ->
  Karma.findOne {'chat': chat, 'name': new RegExp('^'+name+'$', "i")}, (err, karma) ->
    console.log 'err: ' + err if err
    if karma is null
      karma = new Karma()
      karma.name = name
      karma.chat = chat
    callback karma

exports.increment = (chat, name, callback) ->
  findKarma chat, name, (karma) ->
    karma.quantity++;
    karma.save (err) ->
      callback err, karma

exports.decrement = (chat, name, callback) ->
  findKarma chat, name, (karma) ->
    karma.quantity--
    karma.save (err) ->
      callback err, karma

exports.getAll = (chat, callback) ->
  Karma.find {'chat': chat}, (err, karmas) ->
    callback(err, karmas)