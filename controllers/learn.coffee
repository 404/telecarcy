Learn = require '../models/learn'

exports.add = (regex, reply, groups, callback) ->
  learn = new Learn()
  learn.regex = regex
  learn.reply = reply
  learn.groups = groups
  learn.save (err) ->
    callback err

exports.getAll = (callback) ->
  Learn.find (err, replies) ->
    callback err, replies

exports.del = (regex, callback) ->
  Learn.findOneAndRemove {regex: regex}, (err, reply) ->
    callback err, reply
