mongoose = require 'mongoose'

KarmaSchema = new mongoose.Schema {
  chat:
    {
      type: String
      required: true
    }
  name:
    {
      type: String
      required: true
    }
  quantity:
    {
      type: Number
      default: 0
    }
}

module.exports = mongoose.model 'Karma', KarmaSchema