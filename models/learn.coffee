mongoose = require 'mongoose'

LearnSchema = new mongoose.Schema {
  regex:
    {
      type: String
      required: true
    }
  reply:
    {
      type: String
      required: true
    }
  groups:
    {
      type: [Number]
      required: false
    }
}

module.exports = mongoose.model 'Learn', LearnSchema
